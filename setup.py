import setuptools
import pyqt5_installer
from os import path

here = path.abspath(path.dirname(__file__))

with open(path.join(here, 'README.rst')) as f:
    long_desc = f.read()

setuptools.setup(
    name='pyqt5-installer',
    version=pyqt5_installer.__version__,
    description='This package installs an script to install PyQt5',
    long_description=long_desc,
    url='https://pypi.python.org/pypi/pyqt5-installer',
    author='Pepe Osca',
    author_email='',
    license='GPL',
    packages=['pyqt5_installer'],
    install_requires=['wget', 'blessings', 'docopt'],
    zip_safe=False,
    scripts=['bin/install_pyqt5.py'],
    classifiers=[
        'Development Status :: 5 - Production/Stable',
        'Environment :: X11 Applications :: Qt',
        'Intended Audience :: Developers',
        'Intended Audience :: Education',
        'License :: OSI Approved :: GNU General Public License v3 (GPLv3)',
        'Operating System :: MacOS',
        'Operating System :: POSIX :: Linux',
        'Programming Language :: Python :: 3',
        'Topic :: Software Development',
        'Topic :: Software Development :: Libraries :: Application Frameworks'
    ],
    keywords='qt5 pyqt5',
)
